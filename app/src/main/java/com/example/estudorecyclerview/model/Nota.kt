package com.example.estudorecyclerview.model

import java.io.Serializable

class Nota(val titulo: String, val descricao: String) : Serializable {

    override fun toString(): String {
        return "Nota(titulo='$titulo', descricao='$descricao')"
    }
}