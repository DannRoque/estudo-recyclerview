package com.example.estudorecyclerview.helper.callback

import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.RecyclerView
import com.example.estudorecyclerview.ui.activity.dao
import com.example.estudorecyclerview.ui.recyclerview.adapter.ListaNotaAdapter

class NotaItemTouchHelperCallback(private val adapter : ListaNotaAdapter) : ItemTouchHelper.Callback() {

    override fun getMovementFlags(recyclerView: RecyclerView,
        viewHolder: RecyclerView.ViewHolder
    ): Int {
        val marcacoesDeDesliza : Int = ItemTouchHelper.LEFT or ItemTouchHelper.RIGHT
        return  makeMovementFlags(0, marcacoesDeDesliza)
    }

    override fun onMove(recyclerView: RecyclerView,
        viewHolder: RecyclerView.ViewHolder,
        target: RecyclerView.ViewHolder
    ): Boolean {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun onSwiped(viewHolder: RecyclerView.ViewHolder, direction: Int) {
        val posicaoDeslizada = viewHolder.adapterPosition
        dao.remove(posicaoDeslizada)
        adapter.remove(posicaoDeslizada)
    }


}
