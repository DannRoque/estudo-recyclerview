package com.example.estudorecyclerview.ui.activity

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.widget.EditText
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import com.example.estudorecyclerview.R
import com.example.estudorecyclerview.dao.NotaDAO
import com.example.estudorecyclerview.model.Nota
import kotlinx.android.synthetic.main.activity_formulario_nota.*

val dao = NotaDAO()

class FormularioNotaActivity : AppCompatActivity() {

    private var posicaoRecebida = POSICAO_INVALIDA

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_formulario_nota)
        title= TITULO_APPBAR_INSERE

        val dadosRecebidos = intent
        if (dadosRecebidos.hasExtra(CHAVE_NOTA)) {
            title= TITULO_APPBAR_ALTERA
            posicaoRecebida = dadosRecebidos.getIntExtra(CHAVE_POSICAO, POSICAO_INVALIDA)
            val notaRecebida: Nota = dadosRecebidos.getSerializableExtra(CHAVE_NOTA) as Nota
            preencheCampos(notaRecebida)
        }
    }

    private fun preencheCampos(notaRecebida: Nota) {
        val titulo: TextView = formulario_nota_titulo
        val descricao: TextView = formulario_nota_descricao
        titulo.text = notaRecebida.titulo
        descricao.text = notaRecebida.descricao
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu_formulario_nota_salva, menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (ehMenuSalvaNota(item)) {
            val notaCriada = criaNota()
            retornaNota(notaCriada)
            finish()
        }
        return super.onOptionsItemSelected(item)
    }

    private fun retornaNota(notaCriada: Nota) {
        val intentResultado = Intent()
        intentResultado.putExtra(CHAVE_NOTA, notaCriada)
        intentResultado.putExtra(CHAVE_POSICAO, posicaoRecebida)
        setResult(Activity.RESULT_OK, intentResultado)
    }

    private fun criaNota(): Nota {
        val titulo: EditText = formulario_nota_titulo
        val descricao: EditText = formulario_nota_descricao
        return Nota(titulo = titulo.text.toString(), descricao = descricao.text.toString())
    }

    private fun ehMenuSalvaNota(item: MenuItem) =
        item.itemId == R.id.menu_formulario_nota_ic_salva
}
