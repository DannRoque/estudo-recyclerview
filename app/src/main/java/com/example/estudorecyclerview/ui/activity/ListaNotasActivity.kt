package com.example.estudorecyclerview.ui.activity

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.RecyclerView
import com.example.estudorecyclerview.R
import com.example.estudorecyclerview.dao.NotaDAO
import com.example.estudorecyclerview.helper.callback.NotaItemTouchHelperCallback
import com.example.estudorecyclerview.model.Nota
import com.example.estudorecyclerview.ui.recyclerview.adapter.ListaNotaAdapter
import com.example.estudorecyclerview.ui.recyclerview.adapter.listener.OnItemClickListener
import kotlinx.android.synthetic.main.activity_lista_notas.*

const val CODIGO_REQUISICAO_INSERE_NOTA = 1
const val CODIGO_REQUISICAO_ALTERA_NOTA = 2
const val CHAVE_NOTA = "nota"
const val POSICAO_INVALIDA = -1
const val CHAVE_POSICAO = "posicao"
const val TITULO_APPBAR = "Notas"
const val TITULO_APPBAR_INSERE="Insere Nota"
const val TITULO_APPBAR_ALTERA="Altera Nota"

class ListaNotasActivity : AppCompatActivity() {

    private lateinit var adapter: ListaNotaAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_lista_notas)
        title= TITULO_APPBAR
        val todasNotas: MutableList<Nota> = pegaTodasNotas()
        configuraRecyclerView(todasNotas = todasNotas)
        configuraBotaoInsereNota()
    }

    private fun configuraBotaoInsereNota() {
        val insereNota = lista_notas_insere_nota
        insereNota.setOnClickListener {
            vaiParaFormularioNotaActivityInsere()
        }
    }

    private fun vaiParaFormularioNotaActivityInsere() {
        val intent = Intent(this, FormularioNotaActivity::class.java)
        startActivityForResult(intent, CODIGO_REQUISICAO_INSERE_NOTA)
    }

    private fun pegaTodasNotas(): MutableList<Nota> {
        val dao = NotaDAO()
        return dao.todos()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?  ) {
        if (ehResultadoInsereNota(requestCode, data)) {
            if (resultadoOK(resultCode)){
                val notaRecebida: Nota = data?.getSerializableExtra(CHAVE_NOTA) as Nota
                adapter.adiciona(notaRecebida)
            }
        }
        super.onActivityResult(requestCode, resultCode, data)

        if (ehResultadoAlteraNota(requestCode, data)) {
            if (resultadoOK(resultCode)){
                val notaRecebida: Nota = data?.getSerializableExtra(CHAVE_NOTA) as Nota
                val posicaoRecebida = data.getIntExtra(CHAVE_POSICAO, POSICAO_INVALIDA)
                if(ehPosicaoValida(posicaoRecebida)){
                    altera(posicaoRecebida, notaRecebida)
                }else{
                    Toast.makeText(this, "Ocorreu um problema na alteração da nota", Toast.LENGTH_LONG).show()
                }
            }

        }
    }

    private fun altera(posicao: Int, nota: Nota) {
        dao.altera(posicao, nota)
        adapter.altera(posicao, nota)
    }

    private fun ehPosicaoValida(posicaoRecebida: Int) = posicaoRecebida > POSICAO_INVALIDA

    private fun ehResultadoAlteraNota(
        requestCode: Int,
        data: Intent?
    ) =
        (requestCode == CODIGO_REQUISICAO_ALTERA_NOTA) and temNota(
            data
        )

    private fun ehResultadoInsereNota(
        requestCode: Int,
        data: Intent?
    ) = ehCodigoRequisicaoInsereNota(requestCode) and
            temNota(data)

    private fun temNota(data: Intent?) = (data != null) and (data!!.hasExtra(CHAVE_NOTA))

    private fun resultadoOK(resultCode: Int) =
        (resultCode == Activity.RESULT_OK)

    private fun ehCodigoRequisicaoInsereNota(requestCode: Int) =
        (requestCode == CODIGO_REQUISICAO_INSERE_NOTA)

    private fun configuraRecyclerView(todasNotas: MutableList<Nota>) {
        val listaNotas: RecyclerView = lista_nota_recyclerview
        configuraAdapter(listaNotas, todasNotas)
        var itemTouchHelper = ItemTouchHelper(NotaItemTouchHelperCallback(adapter))
        itemTouchHelper.attachToRecyclerView(listaNotas)
    }

    private fun configuraAdapter(
        listaNotas: RecyclerView,
        todasNotas: MutableList<Nota>) {
        adapter = ListaNotaAdapter(todasNotas, this)
        listaNotas.adapter = adapter
        adapter.onItemClickListener = object : OnItemClickListener {
            override fun OnItemClick(nota: Nota, posicao: Int) {
                vaiParaFormularioNotaActivityAltera(nota, posicao)
            }

            private fun vaiParaFormularioNotaActivityAltera(
                nota: Nota,
                posisao: Int
            ) {
                val abreFormularioComNota =
                    Intent(this@ListaNotasActivity, FormularioNotaActivity::class.java)
                abreFormularioComNota.putExtra(CHAVE_NOTA, nota)
                abreFormularioComNota.putExtra(CHAVE_POSICAO, posisao)
                startActivityForResult(abreFormularioComNota, CODIGO_REQUISICAO_ALTERA_NOTA)
            }
        }
    }
}
