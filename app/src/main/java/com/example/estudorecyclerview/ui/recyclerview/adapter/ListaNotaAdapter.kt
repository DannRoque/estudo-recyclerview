package com.example.estudorecyclerview.ui.recyclerview.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.estudorecyclerview.R
import com.example.estudorecyclerview.model.Nota
import com.example.estudorecyclerview.ui.activity.dao
import com.example.estudorecyclerview.ui.recyclerview.adapter.listener.OnItemClickListener
import kotlinx.android.synthetic.main.item_nota.view.*

class ListaNotaAdapter(
    private val notas: MutableList<Nota>,
    private val context: Context
) : RecyclerView.Adapter<ListaNotaAdapter.NotaViewHolder>() {

    lateinit var onItemClickListener: OnItemClickListener

    override fun getItemCount(): Int {
        return notas.size
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): NotaViewHolder {
        val viewCriada = LayoutInflater.from(context).inflate(R.layout.item_nota, parent, false)
        return NotaViewHolder(viewCriada)
    }

    override fun onBindViewHolder(holder: NotaViewHolder, position: Int) {
        holder.bind(notas[position])
    }

    inner class NotaViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        private var tituloCampo: TextView = itemView.item_nota_titulo
        private var descricaoCampo: TextView = itemView.item_nota_descricao

        fun bind(nota: Nota) {
            with(nota) {
                tituloCampo.text = titulo
                descricaoCampo.text = descricao
                itemView.setOnClickListener {
                    onItemClickListener.OnItemClick(nota, posicao = layoutPosition)
                }
            }
        }
    }

    fun adiciona(nota: Nota) {
        dao.insere(nota)
        notifyDataSetChanged()
    }

    fun altera(posicao: Int, nota: Nota) {
        notas[posicao] = nota
        notifyDataSetChanged()
    }

    fun remove(posicao: Int) {
        notas.removeAt(posicao)
        notifyItemRemoved(posicao)

    }
}



