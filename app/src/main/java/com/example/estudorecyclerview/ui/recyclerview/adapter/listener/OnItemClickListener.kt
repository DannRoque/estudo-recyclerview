package com.example.estudorecyclerview.ui.recyclerview.adapter.listener

import com.example.estudorecyclerview.model.Nota

interface OnItemClickListener {
    fun OnItemClick(nota:Nota, posicao : Int)
}