package com.example.estudorecyclerview.dao

import com.example.estudorecyclerview.model.Nota
import java.util.*
import kotlin.collections.ArrayList


private val dao = NotaDAO()
class NotaDAO {

    private val notas: ArrayList<Nota> = ArrayList()

    fun todos(): MutableList<Nota> {
        return notas
    }

    fun insere(notas: Nota) {
        dao.notas.addAll(listOf(notas))
    }

    fun altera(posicao: Int, nota: Nota) {
        notas[posicao] = nota
    }

    fun remove(posicao: Int) {
        notas.removeAt(posicao)
    }

    fun troca(posicaoInicio: Int, posicaoFim: Int) {
        Collections.swap(notas, posicaoInicio, posicaoFim)
    }

    fun removeTodos() {
        notas.clear()
    }
}